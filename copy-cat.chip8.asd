(asdf:defsystem #:copy-cat.chip8
  :description "nothing"
  :license "Cooperative Non-Violent Public License v4+"
  :depends-on ("copy-cat.instruction-database" "copy-cat.cpu" "cl-opengl" "cl-glfw3")
  :components ((:file "instruction-set")
               (:file "cpu")
               (:file "machine")
               (:file "interpreter")
               (:file "graphics")))
