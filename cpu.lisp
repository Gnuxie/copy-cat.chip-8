#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:chip-8.cpu
  (:use #:cl)
  (:local-nicknames (#:cc.cpu #:copy-cat.cpu)))

(in-package #:chip-8.cpu)

(cc.cpu:define-abstract-cpu abstract-chip-8-cpu
    (:copy-cat.chip8 "chip8.assoc"))


