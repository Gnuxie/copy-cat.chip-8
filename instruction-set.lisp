#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:chip-8.instruction-set
  (:use #:cl)
  (:local-nicknames (#:cc.id #:copy-cat.instruction-database)))

(in-package #:chip-8.instruction-set)

(cc.id:define-instruction-set "chip-8"
    (:copy-cat.chip8 "chip8.assoc"))
