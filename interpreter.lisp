#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:chip-8.machine)

(define-interpreter-instruction (c.i:ix0 instruction) (addr)
  (when (= 0 addr)
    (error "Got empty sys call")))

(define-interpreter-instruction (c.i:ixE0 instruction) ()
  (fill video 0)
  (setf video-dirty-p t))

(define-interpreter-instruction (c.i:ixEE instruction) ()
  (setf program-counter (vector-pop stack)))

(define-interpreter-instruction (c.i:ix1000 instruction) (addr)
  (setf program-counter addr))

(define-interpreter-instruction (c.i:ix2000 instruction) (addr)
  (vector-push program-counter stack)
  (setf program-counter addr))

(define-interpreter-instruction (c.i:ix3000 instruction) (reg const)
  (when (= (aref general-purpose reg)
           const)
    (incf program-counter 2)))

(define-interpreter-instruction (c.i:ix4000 instruction) (reg const)
  (unless (= (aref general-purpose reg)
           const)
    (incf program-counter 2)))

(define-interpreter-instruction (c.i:ix5000 instruction) (reg1 reg2)
  (when (= (aref general-purpose reg1)
           (aref general-purpose reg2))
    (incf program-counter 2)))

(define-interpreter-instruction (c.i:ix6000 instruction) (reg const)
  (setf (aref general-purpose reg)
        const))

(define-interpreter-instruction (c.i:ix7000 instruction) (reg const)
  (let ((result (+ (aref general-purpose reg)
                   const)))
    (setf (aref general-purpose reg)
          (ldb (byte 8 0) result))
    (setf (aref general-purpose #xF)
          (if (< 0 (- result 255))
              1
              0))))

(define-interpreter-instruction (c.i:ix8000 instruction) (reg1 reg2)
  (setf (aref general-purpose reg1)
        (aref general-purpose reg2)))

(define-interpreter-instruction (c.i:ix8001 instruction) (reg1 reg2)
  (setf (aref general-purpose reg1)
        (logior (aref general-purpose reg1)
                (aref general-purpose reg2))))

(define-interpreter-instruction (c.i:ix8002 instruction) (Vx Vy)
  (setf (aref general-purpose Vx)
        (logand (aref general-purpose Vx)
                (aref general-purpose Vy))))

(define-interpreter-instruction (c.i:ix8003 instruction) (Vx Vy)
  (setf (aref general-purpose Vx)
        (logxor (aref general-purpose Vx)
                (aref general-purpose Vy))))

(define-interpreter-instruction (c.i:ix8004 instruction) (Vx Vy)
  (let ((result (+ (aref general-purpose Vx)
                   (aref general-purpose Vy))))
    (setf (aref general-purpose Vx)
          (ldb (byte 8 0) result))
    (setf (aref general-purpose 15)
          (if (< 0 (- result 255))
              1
              0))))

(define-interpreter-instruction (c.i:ix8005 instruction) (Vx Vy)
  (let ((result (- (aref general-purpose Vx)
                   (aref general-purpose Vy))))
    (setf (aref general-purpose Vx)
          (ldb (byte 8 0) result))
    (setf (aref general-purpose #xF)
          (if (> Vx Vy)
              1
              0))))

;;; why is Vy an argument to SHR Vx, {Vy} ?
(define-interpreter-instruction (c.i:ix8006 instruction) (Vx Vy)
  (let ((reg-val (aref general-purpose Vx)))
    (setf (aref general-purpose #xF)
          (if (= 1 (ldb (byte 1 0) reg-val)) 1 0))
    (setf (aref general-purpose Vx)
          (ash reg-val -1))))

(define-interpreter-instruction (c.i:ix8007 instruction) (Vx Vy)
  (let ((result (- (aref general-purpose Vy)
                   (aref general-purpose Vx))))
    (setf (aref general-purpose Vx)
          (ldb (byte 8 0) result))
    (setf (aref general-purpose)
          (if (> Vy Vx)
              1
              0))))

(define-interpreter-instruction (c.i:ix800E instruction) (Vx Vy)
  (let ((reg-val (aref general-purpose Vx)))
    (setf (aref general-purpose #xF)
          (if (= 1 (ldb (byte 1 7) reg-val)) 1 0))
    (setf (aref general-purpose Vx)
          (ldb (byte 8 0) (ash reg-val 1)))))

(define-interpreter-instruction (c.i:ix9000 instruction) (Vx Vy)
  (unless (= (aref general-purpose Vx)
             (aref general-purpose Vy))
    (setf program-counter
          (dpb (+ 2 program-counter)
               (byte 12 0)
               program-counter))))

(define-interpreter-instruction (c.i:ixA000 instruction) (addr)
  (setf i addr))

(define-interpreter-instruction (c.i:ixB000 instruction) (addr)
  (setf program-counter
        (ldb (byte 12 0)
             (+ (aref general-purpose #x0)
                addr))))

(define-interpreter-instruction (c.i:ixB000 instruction) (addr)
  (setf program-counter
        (ldb (byte 12 0)
             (+ (aref general-purpose #x0)
                addr))))

(define-interpreter-instruction (c.i:ixC000 instruction) (Vx const)
  (setf (aref general-purpose Vx)
        (logand (random 256) const)))

(define-interpreter-instruction (c.i:ixD000 instruction) (Vx Vy size)
  (setf (aref general-purpose #xF) 0)
  (let ((x-pos (aref general-purpose Vx))
        (y-pos (aref general-purpose Vy)))
    (loop :for row :from 0 :below size
       :for sprite-byte := (aref memory (+ i row))
       :do (loop :for sprite-col :from 7 :downto 0
                :for screen-col :from 0 :below 7
              :do (let* ((sprite-pixel (plusp (ldb (byte 1 sprite-col) sprite-byte)))
                         (x (mod (+ x-pos screen-col) +screen-width+))
                         (y (mod (+ y-pos row) +screen-height+))
                         (screen-pixel (plusp (vref video x y))))
                    (when (and sprite-pixel screen-pixel)
                      (setf (aref general-purpose #xF) 1))
                    (setf (vref video x y)
                          (if (and (or sprite-pixel screen-pixel)
                                   (not (and sprite-pixel screen-pixel)))
                              255
                              0))))))
  (setf video-dirty-p t))

(define-interpreter-instruction (c.i:ixE09E instruction) (Vx)
  (when (aref keys Vx)
    (incf program-counter 2)))

(define-interpreter-instruction (c.i:ixE0A1 instruction) (Vx)
  (unless (aref keys Vx)
    (incf program-counter 2)))

(define-interpreter-instruction (c.i:ixF007 instruction) (Vx)
  (setf (aref general-purpose Vx)
        delay-timer))

(define-interpreter-instruction (c.i:ixF00A instruction) (Vx)
  (let ((key (position t keys)))
    (if key
        (setf (aref general-purpose Vx) key)
        (decf program-counter 2))))

(define-interpreter-instruction (c.i:ixF015 instruction) (Vx)
  (setf delay-timer (aref general-purpose Vx)))

(define-interpreter-instruction (c.i:ixF018 instruction) (Vx)
  (setf sound-timer (aref general-purpose Vx)))

(define-interpreter-instruction (c.i:ixF01E instruction) (Vx)
  (setf i
        (ldb (byte 12 0)
             (+ i (aref general-purpose Vx)))))

(define-interpreter-instruction (c.i:ixF029 instruction) (Vx)
  (setf i (font-location (aref general-purpose Vx))))

(define-interpreter-instruction (c.i:ixF033 instruction) (Vx)
  (flet ((get-digit (n digit)
           (mod (floor n digit) 10)))
    (let ((register-value (aref general-purpose Vx)))
      (setf (aref memory i) (get-digit register-value 100))
      (setf (aref memory (1+ i))
            (get-digit register-value 10))
      (setf (aref memory (+ 2 i))
            (get-digit register-value 1)))))

(define-interpreter-instruction (c.i:ixF055 instruction) (Vx)
  (loop :for n :from 0 :to Vx
     :do (setf (aref memory (+ i n))
               (aref general-purpose n))))

(define-interpreter-instruction (c.i:ixF065 instruction) (Vx)
  (loop :for n :from 0 :to Vx
     :do (setf (aref general-purpose n)
               (aref memory (+ i n)))))
