#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:chip-8.machine
  (:use #:cl #:chip-8.cpu)
  (:local-nicknames (#:cc.id #:copy-cat.instruction-database)
                    (#:c.i #:chip-8.instruction-set)))

(in-package #:chip-8.machine)

;;; ok we're just going to steal a bunch of stuff from sjl's emulator
;;; https://stevelosh.com/blog/2016/12/chip8-cpu/

(defconstant +cycles-per-second+ 500)
(defconstant +cycles-before-sleep+ 10)
(defconstant +screen-width+ 64)
(defconstant +screen-height+ 32)
(defconstant +memory-size+ (* 1024 4))

;;; no need to use stack-pointer because we have a fill pointer
;;; in the stack array.
(defclass chip-8 (abstract-chip-8-cpu)
  ((memory :initarg :memory :accessor memory
           :type (simple-array (unsigned-byte 8) (4096))
           :initform (make-array 4096 :element-type '(unsigned-byte 8)
                                 :initial-element 0))

   (video :initarg :video :accessor video
          :initform (make-array (* +screen-width+ +screen-height+)
                                :element-type 'fixnum
                                :initial-element 0)
          :type (simple-array fixnum (#. (* +screen-width+ +screen-height+))))

   (running :accessor running :type boolean :initform nil)
   (keys :accessor keys
         :initform (make-array 16 :element-type 'boolean :initial-element nil)
         :type (simple-array boolean (16)))
   (video-dirty-p :initform nil :type boolean :accessor video-dirty-p)
   (stack :accessor stack
          :initform (make-array 16 :element-type '(unsigned-byte 12) :fill-pointer 0)
          :type (vector (unsigned-byte 12) 16))
   (wrap-video-p :accessor wrap-video-p :initform t :type boolean)
   (loaded-rom :accessor loaded-rom)
   (instruction-set :reader instruction-set :initform (make-instance 'c.i:chip-8))))

(declaim (inline vref))
(defun vref (array x y)
  (aref array (+ (* +screen-width+ y) x)))

(defun (setf vref) (new-value array x y)
  (setf (aref array (+ (* +screen-width+ y) x))
        new-value))

(declaim (inline keydown keyup))
(defun keydown (chip key)
  (setf (aref (keys chip) key) t))

(defun keyup (chip key)
  (setf (aref (keys chip) key) nil))

(defmacro with-chip ((chip-8) &body body)
  `(with-accessors ((memory memory) (video video) (running running) (keys keys)
                    (video-dirty-p video-dirty-p) (stack stack)
                    (general-purpose general-purpose) (program-counter program-counter)
                    (stack-pointer stack-pointer) (delay-timer delay-timer)
                    (sound-timer sound-timer) (i i) (wrap-video-p wrap-video-p)
                    (loaded-rom loaded-rom))
       ,chip-8
     ,@body))

(defgeneric interpret (machine instruction))

(defmacro define-interpreter-instruction ((instruction-name instruction-sym)
                                          argument-list &body body)
  `(defmethod interpret ((machine chip-8) (,instruction-sym ,instruction-name))
     (cc.id:with-instruction-arguments ,argument-list ,instruction-sym
       (declare (ignorable ,@ (loop :for arg :in argument-list
                                   :collect arg)))
       (with-chip (machine)
         ,@body))))

(defun font-location (character)
  (+ #x50 (* character 5)))

(defun load-font (chip)
  ;; Thanks http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
  (replace (memory chip)
           #(#xF0 #x90 #x90 #x90 #xF0  ; 0
             #x20 #x60 #x20 #x20 #x70  ; 1
             #xF0 #x10 #xF0 #x80 #xF0  ; 2
             #xF0 #x10 #xF0 #x10 #xF0  ; 3
             #x90 #x90 #xF0 #x10 #x10  ; 4
             #xF0 #x80 #xF0 #x10 #xF0  ; 5
             #xF0 #x80 #xF0 #x90 #xF0  ; 6
             #xF0 #x10 #x20 #x40 #x40  ; 7
             #xF0 #x90 #xF0 #x90 #xF0  ; 8
             #xF0 #x90 #xF0 #x10 #xF0  ; 9
             #xF0 #x90 #xF0 #x90 #x90  ; A
             #xE0 #x90 #xE0 #x90 #xE0  ; B
             #xF0 #x80 #x80 #x80 #xF0  ; C
             #xE0 #x90 #x90 #x90 #xE0  ; D
             #xF0 #x80 #xF0 #x80 #xF0  ; E
             #xF0 #x80 #xF0 #x80 #x80) ; F
           :start1 #x50))

(defun reset (chip)
  (with-chip (chip)
    (fill memory 0)
    (fill general-purpose 0)
    (fill keys nil)
    (fill video 0)
    (load-font chip)
    (replace memory (alexandria:read-file-into-byte-vector loaded-rom)
             :start1 #x200)
    (setf running t
          video-dirty-p t
          program-counter #x200
          delay-timer 0
          sound-timer 0
          (fill-pointer stack) 0))
  t)

(defun emulate-cycle (chip)
  (with-chip (chip)
    (let* ((raw-instruction (dpb (aref memory program-counter)
                                 (byte 8 8)
                                 (aref memory (1+ program-counter))))
           (instruction (cc.id::dispatch (instruction-set chip) raw-instruction)))
      (setf program-counter (ldb (byte 12 0)
                                 (+ program-counter 2)))
            
      (interpret chip instruction))
    (when (< 0 sound-timer)
      (decf sound-timer))
    (when (< 0 delay-timer)
      (decf delay-timer))
    nil))

(defun run-cpu (chip)
  (loop :while (running chip)
     :do (emulate-cycle chip)))
