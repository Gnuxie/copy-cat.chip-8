#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:chip-8.machine)

(defvar *active-chip* nil "idk how to do this otherwise sorry")
(defvar *scale* 8)
(defvar *width* (* *scale* 64))
(defvar *height* (* *scale* 32))

(defun change-key (k)
  (case k
    (:1 #x1)
    (:2 #x2)
    (:3 #x3)
    (:4 #xC)

    (:q #x4)
    (:w #x5)
    (:e #x6)
    (:r #xD)

    (:a #x7)
    (:s #x8)
    (:d #x9)
    (:f #xD)

    (:z #xA)
    (:x #x0)
    (:c #xb)
    (:v #xf)
    (t nil)))

(glfw:def-key-callback chip-key (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :escape)
             (eq action :press))
    (glfw:set-window-should-close))
  (let ((key (change-key key)))
    (when (and key (eq action :press))
      (keydown *active-chip* key))
    (when (and key (eq action :release))
      (keyup *active-chip* key))))

(defun render-screen (chip screen-texture)
  (gl:clear-color 1.0 1.0 0.3 1.0)
  (gl:clear :color-buffer-bit)
  (gl:enable :texture-2d)
  (gl:bind-texture :texture-2d screen-texture)
  (when (video-dirty-p chip)
    (gl:color 1 1 1)
    (setf (video-dirty-p chip) nil)
    (gl:tex-sub-image-2d :texture-2d 0 0 0 64 32 :luminance :unsigned-byte
                         (video chip)))

  (gl:with-primitive :quads
    (gl:tex-coord 0 1)
    (gl:vertex -1 -1 0)
    (gl:tex-coord 1 1)
    (gl:vertex 1 -1 0)
    (gl:tex-coord 1 0)
    (gl:vertex 1 1 0)
    (gl:tex-coord 0 0)
    (gl:vertex -1 1 0))
  (glfw:swap-buffers)
  (glfw:poll-events))


(defun initialize-texture ()
  (let ((handle (gl:gen-texture)))
    (gl:bind-texture :texture-2d handle)
    (gl:tex-image-2d :texture-2d 0 :luminance 64 32 0
                     :luminance
                     :unsigned-byte (cffi:null-pointer))
    (gl:tex-parameter :texture-2d :texture-min-filter :nearest)
    (gl:tex-parameter :texture-2d :texture-mag-filter :nearest)
    handle))

(defun run-gui (chip)
  (let ((*active-chip* chip))
    (glfw:with-init-window (:title "CHIP" :width *width* :height *height*)
      (setf %gl:*gl-get-proc-address* #'glfw:get-proc-address)
      (glfw:set-key-callback 'chip-key)
      (gl:clear-color 0 0 0 0)
      (let ((screen-texture (initialize-texture)))
        (unwind-protect
             (loop :while (and (not (glfw:window-should-close-p)) (running chip))
                :do (progn (emulate-cycle chip)
                           (render-screen chip screen-texture)))
          (gl:delete-texture screen-texture))))))
(defun launch (rom)
  (let ((chip-8 (make-instance 'chip-8)))
    (setf (loaded-rom chip-8)
          rom)
    (reset chip-8)
    (setf (running chip-8) t)
    (run-gui chip-8)))
